module.exports = {
    SIGNATURE: "PMX ",
    
    TEXT_ENCODING: {
        UTF_16_LE: 0,
        UTF_8: 1,
    },

    INDEX_SIZE: {
        BYTE: 1,
        SHORT: 2,
        INT: 4,
    },

    VERTEX_WEIGHT_TYPE: {
        BDEF1: 0,
        BDEF2: 1,
        BDEF4: 2,
        SDEF: 3,
        QDEF: 4,
    },

    MATERIAL_DRAWING_FLAGS: {
        NO_CULL: 0x01,
        GROUND_SHADOW: 0x02,
        DRAW_SHADOW: 0x04,
        RECEIVE_SHADOW: 0x08,
        HAS_EDGE: 0x10,
        VERTEX_COLOR: 0x20,
        POINT_DRAWING: 0x40,
        LINE_DRAWING: 0x80,
    },

    MATERIAL_SPHERE_BLEND_MODE: {
        DISABLED: 0,
        MULTIPLY: 1,
        ADDITIVE: 2,
        ADDITIONAL_VEC4: 3,
    },

    MATERIAL_TOON_REFERENCE: {
        TEXTURE_REFERENCE: 0,
        INTERNAL_REFERENCE: 1,
    },

    BONE_BITFLAG: {
        CONNECT_TO: 0x0001,
        ROTATE: 0x0002,
        MOVE: 0x0004,
        OPERATE: 0x0010,
        IK: 0x0020,
        LOCAL_ATTACH: 0x0080,
        ROTATE_ATTACH: 0x0100,
        MOVE_ATTACH: 0x0200,
        AXIS_FIXED: 0x0400,
        LOCAL_AXIS: 0x0800,
        PHYSICS_MORPH: 0x1000,
        PARENT_MORPH: 0x2000,
    },

    MORPH_TYPE: {
        GROUP: 0,
        VERTEX: 1,
        BONE: 2,
        UV: 3,
        APPENDIX_UV1: 4,
        APPENDIX_UV2: 5,
        APPENDIX_UV3: 6,
        APPENDIX_UV4: 7,
        MATERIAL: 8,
        FLIP: 9,
        IMPULSE: 10,
    },

    MORPH_MATERIAL_OFFSET_TYPE: {
        ADD: 0,
        MULTIPLY: 1,
    },

    MORPH_IMPULSE_FLAG: {
        OFF: 0,
        ON: 1,
    },

    DISPLAY_FRAME_FLAG: {
        COMMON: 0,
        SPECIAL: 1,
    },

    DISPLAY_FRAME_INNER_TARGET: {
        BONE: 0,
        MORPH: 1,
    },

    RIGID_BODY_FIGURE: {
        BALL: 0,
        BOX: 1,
        CAPSULE: 2,
    },

    RIGID_BODY_PHYSICS_MODE: {
        STATIC: 0,
        DYNAMIC: 1,
        DYNAMIC_AND_BONE: 2,
    },

    JOINT_TYPE: {
        GENERIC_6DOF_SPRING: 0,
        GENERIC_6DOF: 1,
        POINT_TO_POINT: 2,
        CONE_TWIST: 3,
        SLIDER: 4,
        HINGE: 5,
    }
}