const ieee754 = require('ieee754');

class FileParser {
    constructor(buffer) {
        this.data = new Uint8Array(buffer);
        this.offset = 0;
    }

    readUint8() {
        return this.data[this.offset++];
    }

    readInt8() {
        let val = this.readUint8();
        return val & 0x80 ? -(~val & 0x7f) - 1 : val;
    }

    readUint16() {
        let val = this.readUint8();
        return val | (this.readUint8() << 8);
    }

    readInt16() {
        let val = this.readUint16();
        return val & 0x8000 ? -(~val & 0x7fff) - 1 : val;
    }

    readUint32() {
        let val = 0;
        for (let i = 0; i < 4; i++) {
            val = val | (this.readUint8() << (8 * i));
        }
        return val;
    }

    readInt32() {
        let val = this.readUint32();
        return val & 0x80000000 ? -~val - 1 : val;
    }

    readFloat() {
        let val = ieee754.read(this.data, this.offset, true, 23, 4);
        this.offset += 4;
        return val;
    }

    readChar() {
        return String.fromCharCode(this.readUint8());
    }

    readString(isUtf8) {
        let len = this.readUint32();
        let str = this.data.slice(this.offset, this.offset + len);
        this.offset += len;
        return new TextDecoder(isUtf8 ? 'utf-8' : 'utf-16le').decode(str);
    }

    readVec2() {
        return [this.readFloat(), this.readFloat()];
    }

    readVec3() {
        return [this.readFloat(), this.readFloat(), this.readFloat()];
    }

    readVec4() {
        return [this.readFloat(), this.readFloat(), this.readFloat(), this.readFloat()];
    }
}

module.exports = FileParser;
