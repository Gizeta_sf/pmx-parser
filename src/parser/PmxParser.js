/**
 * https://gist.github.com/felixjones/f8a06bd48f9da9a4539f
 * https://github.com/capsjac/3dmodels/blob/master/etc/PMX_Spec
 */

const FileParser = require('./Fileparser');
const PmxConst = require('../pmx/PmxConst');

class PmxHeader {
    constructor(parser) {
        this.parser = parser;
        this.signature = parser.readChar() + parser.readChar() + parser.readChar() + parser.readChar();
        this.version = parser.readFloat();

        this.globalsCount = parser.readUint8();
        this.textEncoding = parser.readUint8();
        this.appendixUV = parser.readUint8();
        this.vertexIndexSize = parser.readUint8();
        this.textureIndexSize = parser.readUint8();
        this.materialIndexSize = parser.readUint8();
        this.boneIndexSize = parser.readUint8();
        this.morphIndexSize = parser.readUint8();
        this.rigidBodyIndexSize = parser.readUint8();
    }

    get isUtf8() { return this.textEncoding === PmxConst.TEXT_ENCODING.UTF_8; }
    get appendixUVCount() { return this.appendixUV; }
    get vertexIndexReader() {
        switch (this.vertexIndexSize) {
            case PmxConst.INDEX_SIZE.BYTE:
                return this.parser.readInt8.bind(this.parser);
            case PmxConst.INDEX_SIZE.SHORT:
                return this.parser.readInt16.bind(this.parser);
            case PmxConst.INDEX_SIZE.INT:
                return this.parser.readInt32.bind(this.parser);
        }
    }
    get textureIndexReader() {
        switch (this.textureIndexSize) {
            case PmxConst.INDEX_SIZE.BYTE:
                return this.parser.readInt8.bind(this.parser);
            case PmxConst.INDEX_SIZE.SHORT:
                return this.parser.readInt16.bind(this.parser);
            case PmxConst.INDEX_SIZE.INT:
                return this.parser.readInt32.bind(this.parser);
        }
    }
    get materialIndexReader() {
        switch (this.materialIndexSize) {
            case PmxConst.INDEX_SIZE.BYTE:
                return this.parser.readInt8.bind(this.parser);
            case PmxConst.INDEX_SIZE.SHORT:
                return this.parser.readInt16.bind(this.parser);
            case PmxConst.INDEX_SIZE.INT:
                return this.parser.readInt32.bind(this.parser);
        }
    }
    get boneIndexReader() {
        switch (this.boneIndexSize) {
            case PmxConst.INDEX_SIZE.BYTE:
                return this.parser.readInt8.bind(this.parser);
            case PmxConst.INDEX_SIZE.SHORT:
                return this.parser.readInt16.bind(this.parser);
            case PmxConst.INDEX_SIZE.INT:
                return this.parser.readInt32.bind(this.parser);
        }
    }
    get morphIndexReader() {
        switch (this.morphIndexSize) {
            case PmxConst.INDEX_SIZE.BYTE:
                return this.parser.readInt8.bind(this.parser);
            case PmxConst.INDEX_SIZE.SHORT:
                return this.parser.readInt16.bind(this.parser);
            case PmxConst.INDEX_SIZE.INT:
                return this.parser.readInt32.bind(this.parser);
        }
    }
    get rigidBodyIndexReader() {
        switch (this.rigidBodyIndexSize) {
            case PmxConst.INDEX_SIZE.BYTE:
                return this.parser.readInt8.bind(this.parser);
            case PmxConst.INDEX_SIZE.SHORT:
                return this.parser.readInt16.bind(this.parser);
            case PmxConst.INDEX_SIZE.INT:
                return this.parser.readInt32.bind(this.parser);
        }
    }
}

class PmxModelInfo {
    constructor(parser, header) {
        this.characterName = parser.readString(header.isUtf8);
        this.englishCharacterName = parser.readString(header.isUtf8);
        this.comment = parser.readString(header.isUtf8);
        this.englishComment = parser.readString(header.isUtf8);
    }
}

class PmxVertexData {
    constructor(parser, header) {
        this.vertexCount = parser.readUint32();
        let vertex = [];
        for (let i = 0; i < this.vertexCount; i++) {
            let v = {
                position: parser.readVec3(),
                normal: parser.readVec3(),
                uv: parser.readVec2(),
                appendixUV: [],
            };
            for (let j = 0; j < header.appendixUVCount; j++) {
                v.appendixUV.push(parser.readVec4());
            }
            v.weightType = parser.readUint8();
            switch (v.weightType) {
                case PmxConst.VERTEX_WEIGHT_TYPE.BDEF1:
                    v.weight = {
                        bone1Index: header.boneIndexReader(),
                    };
                    break;
                case PmxConst.VERTEX_WEIGHT_TYPE.BDEF2:
                    v.weight = {
                        bone1Index: header.boneIndexReader(),
                        bone2Index: header.boneIndexReader(),
                        weight1: parser.readFloat(),
                    }
                    break;
                case PmxConst.VERTEX_WEIGHT_TYPE.BDEF4:
                    v.weight = {
                        bone1Index: header.boneIndexReader(),
                        bone2Index: header.boneIndexReader(),
                        bone3Index: header.boneIndexReader(),
                        bone4Index: header.boneIndexReader(),
                        weight1: parser.readFloat(),
                        weight2: parser.readFloat(),
                        weight3: parser.readFloat(),
                        weight4: parser.readFloat(),
                    }
                    break;
                case PmxConst.VERTEX_WEIGHT_TYPE.SDEF:
                    v.weight = {
                        bone1Index: header.boneIndexReader(),
                        bone2Index: header.boneIndexReader(),
                        weight1: parser.readFloat(),
                        C: parser.readVec3(),
                        R0: parser.readVec3(),
                        R1: parser.readVec3(),
                    }
                    break;
                case PmxConst.VERTEX_WEIGHT_TYPE.QDEF:
                    v.weight = {
                        bone1Index: header.boneIndexReader(),
                        bone2Index: header.boneIndexReader(),
                        bone3Index: header.boneIndexReader(),
                        bone4Index: header.boneIndexReader(),
                        weight1: parser.readFloat(),
                        weight2: parser.readFloat(),
                        weight3: parser.readFloat(),
                        weight4: parser.readFloat(),
                    }
                    break;
            }
            v.edgeScale = parser.readFloat();
            vertex.push(v);
        }
        this.vertices = vertex;
    }
}

class PmxSurfaceData {
    constructor(parser, header) {
        this.surfaceCount = parser.readUint32() / 3;
        let surface = [];
        for (let i = 0; i < this.surfaceCount; i++) {
            surface.push({
                vertexIndex: [header.vertexIndexReader(), header.vertexIndexReader(), header.vertexIndexReader()],
            });
        }
        this.surfaces = surface;
    }
}

class PmxTextureData {
    constructor(parser, header) {
        this.textureCount = parser.readUint32();
        let texture = [];
        for (let i = 0; i < this.textureCount; i++) {
            texture.push({
                path: parser.readString(header.isUtf8),
            });
        }
        this.textures = texture;
    }
}

class PmxMaterialData {
    constructor(parser, header) {
        this.materialCount = parser.readUint32();
        let material = [];
        for (let i = 0; i < this.materialCount; i++) {
            let m = {
                name: parser.readString(header.isUtf8),
                englishName: parser.readString(header.isUtf8),
                diffuseColor: parser.readVec4(),
                specularColor: parser.readVec3(),
                specularStrength: parser.readFloat(),
                ambientColor: parser.readVec3(),
                drawingFlags: parser.readUint8(),
                edgeColor: parser.readVec4(),
                edgeScale: parser.readFloat(),
                textureIndex: header.textureIndexReader(),
                sphereIndex: header.textureIndexReader(),
                sphereBlendMode: parser.readUint8(),
                toonReference: parser.readUint8(),
            };
            switch (m.toonReference) {
                case PmxConst.MATERIAL_TOON_REFERENCE.TEXTURE_REFERENCE:
                    m.toonValue = header.textureIndexReader();
                    break;
                case PmxConst.MATERIAL_TOON_REFERENCE.INTERNAL_REFERENCE:
                    m.toonValue = parser.readUint8();
                    break;
            }
            m.metaData = parser.readString(header.isUtf8);
            m.surfaceCount = parser.readInt32();
            material.push(m);
        }
        this.materials = material;
    }
}

class PmxBoneData {
    constructor(parser, header) {
        this.boneCount = parser.readUint32();
        let bone = [];
        for (let i = 0; i < this.boneCount; i++) {
            let b = {
                name: parser.readString(header.isUtf8),
                englishName: parser.readString(header.isUtf8),
                position: parser.readVec3(),
                parentIndex: header.boneIndexReader(),
                morphIndex: parser.readInt32(),
                bitFlags: parser.readUint16(),
            };
            if (b.bitFlags & PmxConst.BONE_BITFLAG.CONNECT_TO) {
                b.connectIndex = header.boneIndexReader();
            } else {
                b.offset = parser.readVec3();
            }
            if (b.bitFlags & (PmxConst.BONE_BITFLAG.ROTATE_ATTACH | PmxConst.BONE_BITFLAG.MOVE_ATTACH)) {
                b.attachParentIndex = header.boneIndexReader();
                b.attachRate = parser.readFloat();
            }
            if (b.bitFlags & PmxConst.BONE_BITFLAG.AXIS_FIXED) {
                b.axisVector = parser.readVec3();
            }
            if (b.bitFlags & PmxConst.BONE_BITFLAG.LOCAL_AXIS) {
                b.xAxisVector = parser.readVec3();
                b.zAxisVector = parser.readVec3();
            }
            if (b.bitFlags & PmxConst.BONE_BITFLAG.PARENT_MORPH) {
                b.parentKey = parser.readInt32();
            }
            if (b.bitFlags & PmxConst.BONE_BITFLAG.IK) {
                b.ikTargetIndex = header.boneIndexReader();
                b.ikLoopCount = parser.readUint32();
                b.ikRadiusLimited = parser.readFloat();
                b.ikLinkCount = parser.readUint32();
                b.ikLinks = [];
                for (let j = 0; j < b.ikLinkCount; j++) {
                    let l = {
                        linkIndex: header.boneIndexReader(),
                    };
                    l.radiusLimited = parser.readUint8();
                    if (l.radiusLimited) {
                        l.lowerVector = parser.readVec3();
                        l.upperVector = parser.readVec3();
                    }
                    b.ikLinks.push(l);
                }
            }
            bone.push(b);
        }
        this.bones = bone;
    }
}

class PmxMorphData {
    constructor(parser, header) {
        this.morphCount = parser.readUint32();
        let morph = [];
        for (let i = 0; i < this.morphCount; i++) {
            let m = {
                name: parser.readString(header.isUtf8),
                englishName: parser.readString(header.isUtf8),
                panel: parser.readUint8(),
                type: parser.readUint8(),
                offsetCount: parser.readUint32(),
                offsetData: [],
            };
            for (let j = 0; j < m.offsetCount; j++) {
                let o = {};
                switch (m.type) {
                    case PmxConst.MORPH_TYPE.GROUP:
                        o.morphIndex = header.morphIndexReader();
                        o.morphRate = parser.readFloat();
                        break;
                    case PmxConst.MORPH_TYPE.VERTEX:
                        o.vertexIndex = header.vertexIndexReader();
                        o.coodinateOffset = parser.readVec3();
                        break;
                    case PmxConst.MORPH_TYPE.BONE:
                        o.boneIndex = header.boneIndexReader();
                        o.distance = parser.readVec3();
                        o.turning = parser.readVec4();
                        break;
                    case PmxConst.MORPH_TYPE.UV:
                    case PmxConst.MORPH_TYPE.APPENDIX_UV1:
                    case PmxConst.MORPH_TYPE.APPENDIX_UV2:
                    case PmxConst.MORPH_TYPE.APPENDIX_UV3:
                    case PmxConst.MORPH_TYPE.APPENDIX_UV4:
                        o.vertexIndex = header.vertexIndexReader();
                        o.uvOffset = parser.readVec4();
                        break;
                    case PmxConst.MORPH_TYPE.MATERIAL:
                        o.materialIndex = header.materialIndexReader();
                        o.offsetType = parser.readUint8();
                        o.diffuseColor = parser.readVec4();
                        o.specularColor = parser.readVec3();
                        o.specularStrength = parser.readFloat();
                        o.ambientColor = parser.readVec3();
                        o.edgeColor = parser.readVec4();
                        o.edgeScale = parser.readFloat();
                        o.textureMod = parser.readVec4();
                        o.sphereMod = parser.readVec4();
                        o.toonMod = parser.readVec4();
                        break;
                    case PmxConst.MORPH_TYPE.FLIP:
                        o.morphIndex = header.morphIndexReader();
                        o.morphRate = parser.readFloat();
                        break;
                    case PmxConst.MORPH_TYPE.IMPULSE:
                        o.rigidBodyIndex = header.rigidBodyIndexReader();
                        o.flag = parser.readUint8();
                        o.speed = parser.readVec3();
                        o.torque = parser.readVec3();
                        break;
                }
                m.offsetData.push(o);
            }
            morph.push(m);
        }
        this.morphs = morph;
    }
}

class PmxDisplayFrameData {
    constructor(parser, header) {
        this.displayFrameCount = parser.readUint32();
        let displayFrame = [];
        for (let i = 0; i < this.displayFrameCount; i++) {
            let d = {
                name: parser.readString(header.isUtf8),
                englishName: parser.readString(header.isUtf8),
                flags: parser.readUint8(),
                innerCount: parser.readUint32(),
                innerData: [],
            };
            for (let j = 0; j < d.innerCount; j++) {
                let inn = {
                    type: parser.readUint8(),
                };
                switch (inn.type) {
                    case PmxConst.DISPLAY_FRAME_INNER_TARGET.BONE:
                        inn.boneIndex = header.boneIndexReader();
                        break;
                    case PmxConst.DISPLAY_FRAME_INNER_TARGET.MORPH:
                        inn.morphIndex = header.morphIndexReader();
                        break;
                }
                d.innerData.push(inn);
            }
            displayFrame.push(d);
        }
        this.displayFrames = displayFrame;
    }
}

class PmxRigidBodyData {
    constructor(parser, header) {
        this.rigidBodyCount = parser.readUint32();
        let rigidBody = [];
        for (let i = 0; i < this.rigidBodyCount; i++) {
            let r = {
                name: parser.readString(header.isUtf8),
                englishName: parser.readString(header.isUtf8),
                boneIndex: header.boneIndexReader(),
                group: parser.readInt8(),
                noCollisionGroup: parser.readUint16(),
                figure: parser.readUint8(),
                size: parser.readVec3(),
                position: parser.readVec3(),
                radius: parser.readVec3(),
                mass: parser.readFloat(),
                movingAttenuation: parser.readFloat(),
                radiusAttenuation: parser.readFloat(),
                bounceForce: parser.readFloat(),
                fricticalForce: parser.readFloat(),
                mode: parser.readUint8(),
            };
            rigidBody.push(r);
        }
        this.rigidBodies = rigidBody;
    }
}

class PmxJointData {
    constructor(parser, header) {
        this.jointCount = parser.readUint32();
        let joint = [];
        for (let i = 0; i < this.jointCount; i++) {
            let j = {
                name: parser.readString(header.isUtf8),
                englishName: parser.readString(header.isUtf8),
                type: parser.readUint8(),
                rigidAIndex: parser.readInt8(),
                rigidBIndex: parser.readInt8(),
                position: parser.readVec3(),
                radius: parser.readVec3(),
                positionLowerVector: parser.readVec3(),
                positionUpperVector: parser.readVec3(),
                radiusLowerVector: parser.readVec3(),
                radiusUpperVector: parser.readVec3(),
                bounceMoving: parser.readVec3(),
                bounceRadius: parser.readVec3(),
            };
            joint.push(j);
        }
        this.joints = joint;
    }
}

class PmxSoftBodyData {
    constructor(parser, header) {
        this.softBodyCount = parser.readUint32();
        let softBody = [];
        // TODO
        this.softBodies = softBody;
    }
}

class PmxParser extends FileParser {
    constructor(buffer) {
        super(buffer);
    }

    parse() {
        let header = new PmxHeader(this);
        return {
            header,
            modelInfo: new PmxModelInfo(this, header),
            vertexData: new PmxVertexData(this, header),
            surfaceData: new PmxSurfaceData(this, header),
            textureData: new PmxTextureData(this, header),
            materialData: new PmxMaterialData(this, header),
            boneData: new PmxBoneData(this, header),
            morphData: new PmxMorphData(this, header),
            displayFrameData: new PmxDisplayFrameData(this, header),
            rigidBodyData: new PmxRigidBodyData(this, header),
            jointData: new PmxJointData(this, header),
            softBodyData: new PmxSoftBodyData(this, header),
        };
    }
}

module.exports = PmxParser;
